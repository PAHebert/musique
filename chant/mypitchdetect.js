/*
The MIT License (MIT)

Copyright (c) 2014 Chris Wilson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

window.AudioContext = window.AudioContext || window.webkitAudioContext;

var yop = 0;
var micActive = false;
var audioContext = null;
var audioStream = null;
var isPlaying = false;
var sourceNode = null;
var analyser = null;
var theBuffer = null;
var DEBUGCANVAS = null;
var micElem = null;
var detectorElem, 
    canvasElem,
    waveCanvas,
    pitchElem,
    noteElem,
    detuneElem,
    detuneAmount;

window.onload = function() {
    micElem = document.getElementById( "mic" );
    detectorElem = document.getElementById( "detector" );
    canvasElem = document.getElementById( "output" );
    DEBUGCANVAS = document.getElementById( "waveform" );
    if (DEBUGCANVAS) {
        waveCanvas = DEBUGCANVAS.getContext("2d");
        waveCanvas.strokeStyle = "black";
        waveCanvas.lineWidth = 1;
    }
    pitchElem = document.getElementById( "pitch" );
    noteElem = document.getElementById( "note" );
    detuneElem = document.getElementById( "detune" );
    detuneAmount = document.getElementById( "detune_amt" );

}

function loadAudio() {
    if (audioContext != null)
        return;
    audioContext = new AudioContext();
    MAX_SIZE = Math.max(4,Math.floor(audioContext.sampleRate/5000));	// corresponds to a 5kHz signal
    var request = new XMLHttpRequest();
    request.open("GET", "../sounds/whistling3.ogg", true);
    request.responseType = "arraybuffer";
    request.onload = function() {
        audioContext.decodeAudioData( request.response, function(buffer) { 
            theBuffer = buffer;
        } );
    }
    request.send();
}

function error() {
    alert('Stream generation failed.');
}

function getUserMedia(constraints, callback) {
    try {
        navigator.mediaDevices.getUserMedia(constraints).then(function (stream)
            {
                callback(stream);
            });
    } catch(err)
    {
        console.log(err);
    }
}

function gotStream(stream) {
    // Create an AudioNode from the stream.
    audioStream = stream;
    var mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // Connect it to the destination.
    analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    mediaStreamSource.connect( analyser );
}

function toggleOscillator() {
    if (isPlaying) {
        //stop playing and return
        sourceNode.stop( 0 );
        sourceNode = null;
        analyser = null;
        isPlaying = false;
        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = window.webkitCancelAnimationFrame;
        window.cancelAnimationFrame( rafID );
        return "play oscillator";
    }
    sourceNode = audioContext.createOscillator();
    sourceNode.type = "sinus";
    sourceNode.frequency.value = 440;

    analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    sourceNode.connect( analyser );
    analyser.connect( audioContext.destination );
    sourceNode.start(0);
    isPlaying = true;
    isLiveInput = false;

    //pitchComputing();
    updatePitch();

    return "stop";
}

function toggleLiveInput() {
    if (isPlaying) {
        //stop playing and return
        sourceNode.stop( 0 );
        sourceNode = null;
        analyser = null;
        isPlaying = false;
        //if (!window.cancelAnimationFrame)
        //window.cancelAnimationFrame = window.webkitCancelAnimationFrame;
        //window.cancelAnimationFrame( rafID );
    }

    if (!micActive)
    {
        getUserMedia(
            {
                audio: true, video: false, audio: {
                    optional: [
                        {echoCancellation: false},
                        {mozAutoGainControl: false},
                        {mozNoiseSuppression: false},
                        {googEchoCancellation: false},
                        {googAutoGainControl: true},
                        {googNoiseSuppression: false},
                        {googHighpassFilter: false}
                    ]
                }
            }, gotStream);
        micActive = !micActive;
        updatePitch();
    } else
    {
        if (audioStream)
        {
            audioStream.getAudioTracks().forEach(track => {
                track.stop();
            });
            micActive = !micActive;
        }
    }
    if (micActive)
        mic.style.backgroundColor = 'lightblue';
    else
        mic.style.backgroundColor = '';
    /*{
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream);*/
    //pitchComputing();
}

function togglePlayback() {
    if (isPlaying) {
        //stop playing and return
        sourceNode.stop( 0 );
        sourceNode = null;
        analyser = null;
        isPlaying = false;
        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = window.webkitCancelAnimationFrame;
        window.cancelAnimationFrame( rafID );
        return "start";
    }

    sourceNode = audioContext.createBufferSource();
    sourceNode.buffer = theBuffer;
    sourceNode.loop = true;

    analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    sourceNode.connect( analyser );
    analyser.connect( audioContext.destination );
    sourceNode.start( 0 );
    isPlaying = true;
    isLiveInput = false;
    updatePitch();

    return "stop";
}

var rafID = null;
var tracks = null;
var buflen = 1024;
var buf = new Float32Array( buflen );
var bufCopie = new Float32Array( buflen );

var noteStrings = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

function moduloOctaveFromNoteToPitch(note, freq)
{
    var noteRef = noteFromPitch(freq);
    if (note <noteRef)
        while(Math.abs(note-noteRef)>=12)
            note+=12;
    else
        while(Math.abs(note-noteRef)>=12)
            note-=12;
    return note;
}

function noteFromPitch( frequency ) {
    var noteNum = 12 * (Math.log( frequency / 440 )/Math.log(2) );
    return Math.round( noteNum ) + 69;
}

function frequencyFromNoteNumber( note ) {
    return 440 * Math.pow(2,(note-69)/12);
}

function centsOffFromPitch( frequency, note ) {
    return Math.floor( 1200 * Math.log( frequency / frequencyFromNoteNumber( note ))/Math.log(2) );
}

// this is a float version of the algorithm below - but it's not currently used.
/*
function autoCorrelateFloat( buf, sampleRate ) {
    var MIN_SAMPLES = 4;	// corresponds to an 11kHz signal
    var MAX_SAMPLES = 1000; // corresponds to a 44Hz signal
    var SIZE = 1000;
    var best_offset = -1;
    var best_correlation = 0;
    var rms = 0;

    if (buf.length < (SIZE + MAX_SAMPLES - MIN_SAMPLES))
        return -1;  // Not enough data

    for (var i=0;i<SIZE;i++)
        rms += buf[i]*buf[i];
    rms = Math.sqrt(rms/SIZE);

    for (var offset = MIN_SAMPLES; offset <= MAX_SAMPLES; offset++) {
        var correlation = 0;

        for (var i=0; i<SIZE; i++) {
            correlation += Math.abs(buf[i]-buf[i+offset]);
        }
        correlation = 1 - (correlation/SIZE);
        if (correlation > best_correlation) {
            best_correlation = correlation;
            best_offset = offset;
        }
    }
    if ((rms>0.1)&&(best_correlation > 0.1)) {
        console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")");
    }
    //	var best_frequency = sampleRate/best_offset;
}
*/

    var MIN_SAMPLES = 40;  // will be initialized when AudioContext is created.
    var GOOD_ENOUGH_CORRELATION = 0.85; // this is the "bar" for how close a correlation needs to be
    var SEUIL_RMS = 0.03;
    var SEUIL_LOCAL_CORREL = 0.2;
    var correlations = null;

    function autoCorrelate( buf, sampleRate ) {
        var SIZE = buf.length;
        var MAX_SAMPLES = Math.floor(SIZE/2);
        var best_offset = -1;
        var best_correlation = -2;
        var best_local_correlation = -2;
        var best_local_correlations = [];
        var foundGoodCorrelation = 0;
        if (correlations==null)
            correlations = new Array(MAX_SAMPLES);
        var best_local_offsets = [];
        var best_local_offset = -2;

        var mean=0;
        var rms = 0;
        for (var i=0;i<SIZE;i++) {
            var val = buf[i];
            bufCopie[i] = val;
            mean += val;
            rms += val*val;
        }
        mean = mean/SIZE;
        rms = Math.sqrt(rms/SIZE);
        //console.log("rms = " + rms);

        if (rms<SEUIL_RMS) // not enough signal
            return 0; //silence

        var variance=0;
        for (var i=0;i<SIZE;i++) {
            bufCopie[i] -= mean;
            variance += bufCopie[i]*bufCopie[i];
        }
        variance = variance/SIZE;

        var lastCorrelation=2;
        for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
            var correlation = 0;
            for (var i=0; i<MAX_SAMPLES; i++) {
                correlation += bufCopie[i]*bufCopie[i+offset];
            }
            correlation = correlation/MAX_SAMPLES/variance;
            correlations[offset] = correlation; // store it, for the tweaking we need to do below.

            if (correlation < lastCorrelation)
            { // cas où la corrélation redescend : on retient le maximum local
                if (best_local_correlation>GOOD_ENOUGH_CORRELATION)
                {
                    foundGoodCorrelation++;
                    best_local_correlations.push(best_local_correlation);
                    best_local_offsets.push(best_local_offset);
                    best_local_correlation = -2;
                }
            }
            else 
            { // cas où la corrélation grimpe
                if (correlation>GOOD_ENOUGH_CORRELATION)
                { //bonne corrélation trouvée
                    if (correlation > best_local_correlation)
                    { // mise à jour maximum local
                        best_local_offset = offset;
                        best_local_correlation = correlation;
                    }
                    if (correlation > best_correlation)
                    { // mise à jour maximum global
                        best_correlation = correlation;
                        best_offset = offset;
                    }
                }
            } 
            if (foundGoodCorrelation==2) {
                // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
                // Now we need to tweak the offset - by interpolating between the values to the left and right of the
                // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
                // we need to do a curve fit on correlations[] around best_offset in order to better determine precise
                // (anti-aliased) offset.

                // we know best_offset >=1, 
                // since foundGoodCorrelation cannot go to true until the second pass (offset=1), and 
                // we can't drop into this clause until the following pass (else if).
                best_offset = best_local_offsets[0];
                if ((best_local_correlations[1]-best_local_correlation[0]) > SEUIL_LOCAL_CORREL)
                    best_offset = best_local_offsets[1];
                break;
            }
            lastCorrelation = correlation;
        }
        //console.log("foundGood = " + foundGoodCorrelation + " " + sampleRate/best_offset + " parmi : " + (sampleRate/best_local_offsets[0]) + ";" + sampleRate/best_local_offsets[1] );
        if (foundGoodCorrelation>0)
        {
            var shift = 1;
            if ((best_offset>MIN_SAMPLES)&&(best_offset<(MAX_SAMPLES-1)))
                shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];  

            if (isNaN(sampleRate/(best_offset+(8*shift))))
                alert("piouc1 of: " + best_offset + " sh: " + shift + " co: " + best_correlation + " rms: " + rms);
            return sampleRate/(best_offset+(8*shift));
        }
        //	if (best_correlation > 0.01) {
        //		//console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
        //            if (isNaN(sampleRate/(best_offset)))
        //                alert("piouc2 of: " + best_offset +  " co: " + best_correlation + " rms: " + rms);
        //		return sampleRate/best_offset;
        //	}
        return -1;
    }

var detectedPitch = -1; // fréquence détectée brute
var stablePitchTab = [];// liste des dernières fréquences détectées proches de la fréquence stable
var stablePitch = -1;   // fréquence stable (=moyenne de la liste précédente)
var nextPitchTab = [];  // liste des dernières fréquences détectées proches de la fréquence stable alternative
var nextPitch = -1;     // fréquence alternative à la fréq. stable (=moyenne de la liste précédente)
var allPitches = [];    // liste des fréquences brutes détectées 
var compteurStable = 0; // crédit de la fréquence stable 
var compteurNext = 0;   // crédit de la fréquence alternative
const seuilHautStabilite = 5; // pour qu'une fréquence alternative devienne stable, elle doit être répétée ce nombre de fois consécutivement (sauf non-détections)
const seuilBasStabilite = -10; // en l'absence de fréquence alternative, la fréquence stable reste « stable » tant que son crédit ne descend pas en dessous

var instant = new Date().getTime();

function mean(myarray)
{
    let sum = myarray.reduce((previous, current) => current += previous);
    let avg = sum / myarray.length;
    return avg;
}

function completeTab(myarray, value, taille)
{
    if (myarray.length >= taille)
        myarray.shift();
    myarray.push(value);
}

function comparePitchToRef(pitch_candidate, pitch_reference)
{
    if ((pitch_candidate == 0) || (pitch_reference == 0))
        return pitch_candidate == pitch_reference;

    var note = noteFromPitch( pitch_reference );
    var detune = centsOffFromPitch( pitch_candidate, note );

    return Math.abs(detune)<50;
}

function pitchComputing()
{
    if (analyser==null)
        return;
    yop++;
    analyser.getFloatTimeDomainData( buf );
    detectedPitch = autoCorrelate( buf, audioContext.sampleRate );
    if (!isNaN(detectedPitch) && (detectedPitch!=-1))
    {// note détectée
        if ( (stablePitch!=-1) && comparePitchToRef(detectedPitch, stablePitch))
        { // cas note stable
            compteurStable = seuilHautStabilite; // on la conforte plein pot
            completeTab(stablePitchTab, detectedPitch, seuilHautStabilite);
            stablePitch = mean(stablePitchTab);

            if (compteurNext>0) //note concurrente dévaluée
                compteurNext--;
        }
        else
        { // cas autre note
            if (stablePitch!=-1)
            { //cas où la note stable existe, mais une autre est détectée
                if (compteurStable > seuilBasStabilite)
                    compteurStable--;
                else
                { // cas stabilité perdue complètement
                    stablePitchTab = [];
                    stablePitch = -1;
                }
            }

            if ( (nextPitch!=-1) && comparePitchToRef(detectedPitch, nextPitch))
            { // cas note concurrente re-connue 
                completeTab(nextPitchTab, detectedPitch, seuilHautStabilite);
                nextPitch = mean(nextPitchTab);

                if (compteurNext<seuilHautStabilite)
                { //on la conforte
                    if (compteurNext<0)
                        compteurNext = 0;
                    compteurNext++;
                }
                else 
                { //on la stabilise
                    compteurStable = seuilHautStabilite;
                    compteurNext = 0;
                    stablePitchTab = nextPitchTab;
                    stablePitch = detectedPitch;
                    nextPitchTab = [];
                    nextPitch = -1;
                }
            } 
            else
            { // cas note inconnue
                nextPitch = detectedPitch;
                nextPitchTab = [detectedPitch];
                compteurNext = 1;
            }

        }
        // quoi qu'il arrive on sauve la note détectée
        completeTab(allPitches, detectedPitch, 512);
    }
    else
    { // cas pas de note détectée
        if (compteurStable > seuilBasStabilite)
            compteurStable--;
        else
        {
            stablePitch = -1;
            stablePitchTab = [];
        }

        if (compteurNext > 0)
            compteurNext--;
        else
        {
            nextPitch = -1;
            nextPitchTab = [];
        }
    }
    //console.log(yop + " " + stablePitch + ": " + compteurStable + " | " + nextPitch + ": " + compteurNext );
}

function updatePitch( time ) {
    if (!DEBUGCANVAS)
        return;

    var nouvel_instant = new Date().getTime();
    if ((nouvel_instant-instant)>0)
    {
        instant = nouvel_instant;
        pitchComputing();

        var ac = stablePitch;
        // TODO: Paint confidence meter on canvasElem here.

        if (DEBUGCANVAS) {  // This draws the current waveform, useful for debugging
            waveCanvas.clearRect(0,0,512,256);
            waveCanvas.strokeStyle = "red";
            waveCanvas.beginPath();
            waveCanvas.moveTo(0,0);
            waveCanvas.lineTo(0,256);
            waveCanvas.moveTo(128,0);
            waveCanvas.lineTo(128,256);
            waveCanvas.moveTo(256,0);
            waveCanvas.lineTo(256,256);
            waveCanvas.moveTo(384,0);
            waveCanvas.lineTo(384,256);
            waveCanvas.moveTo(512,0);
            waveCanvas.lineTo(512,256);
            waveCanvas.stroke();
            waveCanvas.strokeStyle = "black";
            waveCanvas.beginPath();
            waveCanvas.moveTo(0,0);

            for (var i=1;i<=allPitches.length;i++) {
                waveCanvas.lineTo(i,256-Math.trunc(allPitches[i-1]/1024.0*256));
            }
            waveCanvas.stroke();
        }

        if (true && (ac <= 0)) 
        { // cas silence ou non-détection
            detectorElem.className = "vague";
            pitchElem.innerText = "--";
            noteElem.innerText = "-";
            detuneElem.className = "";
            detuneAmount.innerText = "--";
        } else {
            detectorElem.className = "confident";
            pitch = ac;
            pitchElem.innerText = Math.round( pitch ) ;
            var note =  noteFromPitch( pitch );

            noteElem.innerHTML = noteStrings[note%12];
            var detune = centsOffFromPitch( pitch, note );
            if (detune == 0 ) {
                detuneElem.className = "";
                detuneAmount.innerHTML = "--";
            } else {
                if (detune < 0)
                    detuneElem.className = "flat";
                else
                    detuneElem.className = "sharp";
                detuneAmount.innerHTML = Math.abs( detune );
            }
        }
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = window.webkitRequestAnimationFrame;
    if (micActive)
        rafID = window.requestAnimationFrame( updatePitch );
}

function pitchLoop()
{
    if (analyser !=null)
    {
        pitchComputing();
        testeNote();
    }
    setTimeout(pitchLoop, 20);
}
