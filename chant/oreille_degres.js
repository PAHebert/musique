var myorange = '#FF8A0A';
var mylightgray = '#EAEAEA';
var mylightblue = '#70A0EA';
var dureeNotes = 1.3;
var delaiEntreNotes = 0.25;
var premierOctave = 4;
var tirageDegre = -1;
var noteDepartMode = 0;
var intervalesMode = [
    [1, 1, 0.5, 1, 1, 1, 0.5],//majeure = ionien
    [1, 0.5, 1, 1, 1, 0.5, 1],//dorien
    [0.5, 1, 1, 1, 0.5, 1, 1], //phrygien
    [1, 1, 1, 0.5, 1, 1, 0.5], //lydien
    [1, 1, 0.5, 1, 1, 0.5, 1], //mixolydien
    [1, 0.5, 1, 1, 0.5, 1, 1], //éolien = mineure naturelle
    [0.5, 1, 1, 0.5, 1, 1, 1], //locrien
    [1, 0.5, 1, 1, 1, 1, 0.5],//mineure mélodique
    [0.5, 1, 1, 1, 1, 0.5, 1],//dorien b9
    [1, 1, 1, 1, 0.5, 1, 0.5],//lydien #5
    [1, 1, 1, 0.5, 1, 0.5, 1],//lydien b7
    [1, 1, 0.5, 1, 0.5, 1, 1],//mixolydien b13
    [1, 0.5, 1, 0.5, 1, 1, 1],//locrien becarre 9
    [0.5, 1, 0.5, 1, 1, 1, 1],//mode altéré
    [1, 0.5, 1, 1, 0.5, 1.5, 0.5],//mineure harmonique
    [0.5, 1, 1, 0.5, 1.5, 0.5, 1],//locrien becarre 13
    [1, 1, 0.5, 1.5, 0.5, 1, 0.5],//ionien #5
    [1, 0.5, 1.5, 0.5, 1, 0.5, 1],//dorien #11
    [0.5, 1.5, 0.5, 1, 0.5, 1, 1],//mixolydien b9b13
    [1.5, 0.5, 1, 0.5, 1, 1, 0.5],//lydien #9
    [0.5, 1, 0.5, 1, 1, 0.5, 1.5]//altéré bb7
];

var notesMode = [];
var posMode = [];//positions
var nomNote = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'];
var nomNoteFr = ['Do','Do#','Re','Re#','Mi','Fa','Fa#','Sol','Sol#','La','La#','Si'];


function initNotesModeApartirIntervales()
{
    var indTypeMode = document.Mode.Type.selectedIndex;
    noteDepartMode = document.Mode.Nom.selectedIndex;
    notesMode = [0 + noteDepartMode];
    posMode = [0];
    for (var degre=2;degre<=(intervalesMode[indTypeMode].length + 1);degre++)//last ignored
    {
        notesMode.push(2*intervalesMode[indTypeMode][degre-2] + notesMode[degre-2]);
        posMode.push(2*intervalesMode[indTypeMode][degre-2] + posMode[degre-2]);
    }
}

function initDegres()
{
    setCouleurDegre(13, 'white');
    for (var i=0;i<13;i++)
    {
        setCouleurDegre(i, mylightgray);
        setTexteDegre(i,'');
    }
    for (var i=0;i<notesMode.length;i++)
    {
        setCouleurDegre(posMode[i], mylightblue);
        //setTexteDegre(posMode[i], i+1);
        setTexteDegre(posMode[i], nomNoteFr[notesMode[i]%12]);
    }
}

function clignoteTirageDegre(etatClignotement)
{
    var couleur = (etatClignotement ? mylightblue : mylightgray);
    if (tirageDegre!=-1)
        setCouleurDegre(posMode[tirageDegre], couleur);

    setTimeout(clignoteTirageDegre, 500, !etatClignotement);
}

function changeMode()
{
    initNotesModeApartirIntervales();
    initDegres();
    joueMode();
    actionJeu(validation=false);
}

function joueMode(degre=0,asc=true)
{
    if ((degre<0)||(degre>=notesMode.length))
        return;

    var note = notesMode[degre];
    pressPropo(posMode[degre]);

    if (asc)
        degre = degre + 1;
    else
        degre = degre - 1;

    if (degre>=notesMode.length)
    {
        asc = false;
        degre = degre - 1;
    }

    setTimeout(joueMode, delaiEntreNotes*1000, degre, asc); 
}

function proposeDegre()
{
    tirageDegre = Math.floor(Math.random() * notesMode.length);
    selectionneNote(tirageDegre);
}

var attenteSilence = true;
function testeNote()
{
    if ((attenteSilence)&&(stablePitch==0))
    { // on ne relance une reconnaissance qu'après un silence
        attenteSilence = false;
    //    console.log("freq perçue = "  + stablePitch + " ; attend silence");
    }
    if ((!attenteSilence) && (stablePitch>0))
    { // on lance la reco si pas trouvé et si y a une note stable

        var ref = moduloOctaveFromNoteToPitch(notesMode[tirageDegre], stablePitch);
        var note =  noteFromPitch( stablePitch );
        var detune = centsOffFromPitch( stablePitch, ref );
        //console.log( ref%12 + " vs " + note%12 + " = " + ref + " vs " + note + " = " + Math.round(stablePitch) + " : " + detune);
        if (detune<50)
        {
            pressPropo(posMode[tirageDegre], silence=true, couleur='green');
            actionJeu(validation=false);
     //       console.log("freq perçue = "  + stablePitch + " ; detune = " + detune + " ; reconnue");
            attenteSilence = true;
        } else
        {
            var posErreur = []; //possibilité de repérer les 2 plus proches
            for (var ind=0;ind<12;ind++)
            {
                note = moduloOctaveFromNoteToPitch(notesMode[0]+ind, stablePitch);
                detune = centsOffFromPitch( stablePitch, note);
                if (Math.abs(detune)<50)
                {
                    posErreur.push(ind%12);
      //              console.log("freq perçue = "  + stablePitch + " ; detune = " + detune + " ; echec" + (ind%12 +1));
                }
            }
            for (var ind=0;ind<posErreur.length;ind++)
                pressPropo(posErreur[ind], silence=true, couleur='red');
        }
    }
}


function actionJeu(validation=true)
{
    if (validation)
        pressPropo(posMode[tirageDegre]);
    setTimeout(proposeDegre, delaiEntreNotes*1000);
}

function keywaspressed(e)
{
    if (document.activeElement.nodeName == 'TEXTAREA') return;
    if (document.activeElement.nodeName == 'INPUT') return;

    e = e || event;
    //document.title = e.keyCode;
    var k = e.keyCode;
    //alert(k);
    if (k==32) {// enter
        actionJeu();
    }
/*    if (k==9)  { press(1); return false; } //tab
    if (k==49) press(2); //1
    if (k==65) press(3); //a 
    if (k==50) press(4); //2
    if (k==90) press(5); //z
    if (k==69) press(6); //e
    if (k==52) press(7); //4
    if (k==82) press(8); //r
    if (k==53) press(9); //5
    if (k==84) press(10); //t
    if (k==54) press(11); //6
    if (k==89) press(12); //y
    if (k==85) press(13); //u
    */
}

function joueNote(x)
{
    var note = nomNote[x%12];
    var octave = Math.trunc(x/12 + premierOctave);
    Synth.play('piano',note, octave, dureeNotes);
}


function selectionneNote(x)
{
    initDegres();
    setCouleurDegre(posMode[x], mylightblue)
}

function pressPropo(x, silence=false, couleur=myorange)
{
    var a = document.getElementById('deg'+x);
    var couleur_initiale = a.style.backgroundColor;
    if (couleur_initiale == couleur)
        return;
    a.style.backgroundColor = couleur;
    if (!silence)
        joueNote(x + noteDepartMode);
    setTimeout(dePressPropo, delaiEntreNotes*1000, a, couleur_initiale);
}

function dePressPropo(id, couleur='white')
{
    id.style.backgroundColor = couleur;
}




function initFormulaire()
{
    document.getElementById('idbody').onkeydown = keywaspressed;

    var containerwidth = document.getElementById('maincontainer').clientWidth;
    var mcwidth = document.getElementById('maincolumn').clientWidth;
    var prawawidth = containerwidth - mcwidth;
    if (prawawidth < 300)
    {
        prawawidth = 0;
        document.getElementById('skyscraper').innerHTML = '';
    }
    if (prawawidth > 320) prawawidth = 320;
    document.getElementById('skyscraper').style.width = prawawidth + 'px';
}

initNotesModeApartirIntervales();
initDegres();
initFormulaire();
actionJeu(validation=false);
clignoteTirageDegre();

